#include <stdlib.h>
#include <stdio.h>
#include "funkcje.h"


int main()
{
    int n=50;
    FILE *plik;
    fopen("P0001_attr.rec.txt", "r");
    //Tworzę trzy tablice dynamiczne//
    float *array_1 = (float *)malloc(n * sizeof(float));
    float *array_2 = (float *)malloc(n * sizeof(float));
    float *array_3 = (float *)malloc(n * sizeof(float));
    if((plik=fopen("P0001_attr.rec.txt", "r"))==NULL)
    
    {
            printf("Nie mozna otworzyc pliku. ");
    }
    else 
    {
        //Pomijam pierwszą linię ze znakami typu char w pliku//
        fscanf(plik, "%*[^\n]\n");
        int i =0;
        //Przechodzę linia po lini i dodaje do odpowiedniej tablicy odpowiadające liczby typu float//
        while (fscanf(plik,"%*f\t%f\t%f\t%f\n", &array_1[i], &array_2[i], &array_3[i])==3)
        {
            //Można wyświetlić sobie utworzone tablice//
           /* printf("%d\n", i);
            printf("%f\n", array_1[i]);
            printf("Tablica 2:\n");
            printf("%f\n", array_2[i]);
            printf("Tablica 3:\n");
            printf("%f\n", array_3[i]);*/
            i++;
        }
        
  
    }
    fclose(plik);

    float m=mean(n, array_1);
    float m2=mean(n, array_2);
    float m3=mean(n, array_3);

    printf("\nWartosc sredniej: %f", m);
    printf("\nWartosc sredniej: %f", m2);
    printf("\nWartosc sredniej: %f", m3);

    float me =  median(n, array_1);
    float me2 = median(n, array_2);
    float me3 = median(n, array_3);

    printf("\nMediana wynosi: %f", me);
    printf("\nMediana wynosi: %f", me2);
    printf("\nMediana wynosi: %f", me3);

    float st=standard_deviation(n, array_1, m);
    float st2=standard_deviation(n, array_2, m2);
    float st3=standard_deviation(n, array_3, m3);

    printf("\nWartosc odchhylenia standardowego wynosi: %f", st);
    printf("\nWartosc odchhylenia standardowego wynosi: %f", st2);
    printf("\nWartosc odchhylenia standardowego wynosi: %f", st3);

    if((plik=fopen("P0001_attr.rec.txt", "a"))==NULL)
    
    {
            printf("Nie mozna otworzyc pliku. ");
    }
    else 
    {
        fprintf(plik,"\n-----------\n");
        fprintf(plik,"Wartosc sredniej dla X wynosi: %f",m);
        fprintf(plik,"\nWartosc sredniej dla Y wynosi: %f", m2);
        fprintf(plik,"\nWartosc sredniej dla RHO wynosi: %f", m3);
        fprintf(plik,"\nMediana dla X wynosi: %f", me);
        fprintf(plik,"\nMediana dla Y wynosi: %f", me2);
        fprintf(plik,"\nMediana dla RHO wynosi: %f", me3);
        fprintf(plik, "\nWartosc odchhylenia standardowego dla X wynosi: %f", st);
        fprintf(plik, "\nWartosc odchhylenia standardowego dla Y wynosi: %f", st2);
        fprintf(plik, "\nWartosc odchhylenia standardowego dla RHO wynosi: %f", st3);
        fprintf(plik, "\n-----------");



    }


    fclose(plik);
    free(array_1);
    free(array_2);
    free(array_3);



    float a = 0.0;
    scanf("%f", &a);

    return 0;
}

