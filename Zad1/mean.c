#include "funkcje.h"
#include <math.h>

float mean(int p_n, float p_x[])
{
    int i = 0;
    float res = 0.0;

    for(i=0; i<p_n; i++)
    {
        res += p_x[i];
    }

    res /= p_n;

    return res;
}