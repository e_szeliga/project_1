#include <stdlib.h>
#include <stdio.h>
#include "funkcje.h"
#include <string.h>
#define n 50

struct data
{
    float X[n];
    float Y[n];
    float RHO[n];
    
};

struct statistic
{
    float Mean[2];
    float Median[2];
    float Stan_deviation[2];
};

int main()
{
    
    struct data data_1;
    struct statistic statistic_1;
    FILE *plik;
    fopen("P0001_attr.rec.txt", "r");

    if((plik=fopen("P0001_attr.rec.txt", "r"))==NULL)
    
    {
            printf("Nie mozna otworzyc pliku. ");
    }
    else 
    {
        fscanf(plik, "%*[^\n]\n");
        int i =0;
        //Przechodzę linia po lini i dodaje do struktury odpowiadające liczby typu float//
        while (fscanf(plik,"%*f\t%f\t%f\t%f\n", &data_1.X[i], &data_1.Y[i], &data_1.RHO[i])==3)
        {
            printf("Tablica 1:");
            printf("%f\n", data_1.X[i]);
            printf("Tablica 2:");
            printf("%f\n", data_1.Y[i]);
            printf("Tablica 3:");
            printf("%f\n", data_1.RHO[i]);
            i++;
        }


    }
    fclose(plik);


    float m=mean(n, data_1.X);
    float m2=mean(n, data_1.Y);
    float m3=mean(n, data_1.RHO);
    
    float me =  median(n, data_1.X);
    float me2 = median(n, data_1.Y);
    float me3 = median(n, data_1.RHO);

    float st=standard_deviation(n, data_1.X, m);
    float st2=standard_deviation(n, data_1.Y, m2);
    float st3=standard_deviation(n, data_1.RHO, m3);

    statistic_1.Mean[0]=m;
    statistic_1.Median[0]=me;
    statistic_1.Stan_deviation[0]=st;
    statistic_1.Mean[1]=m2;
    statistic_1.Median[1]=me2;
    statistic_1.Stan_deviation[1]=st2;
    statistic_1.Mean[2]=m3;
    statistic_1.Median[2]=me3;
    statistic_1.Stan_deviation[2]=st3;

 for(int i = 0; i<3; i++)
 {
     printf("Tablica statystyk %d to: %f\t%f\t%f\n", i ,statistic_1.Mean[i], statistic_1.Median[i], statistic_1.Stan_deviation[i]);
 }

if ((plik = fopen("P0001_attr.rec.txt", "a+"))==NULL)
    printf("Blad otwarcia pliku");
else
{
    char *p;
    char *c = "----";
    rewind(plik);
    int finded = 0;
    char linia[10000] = "";
    while(fgets(linia, 10000, plik))
    {
        p = strstr(linia, c);
        if(p!=NULL)
        {
           finded = 1;
           break;
        }
    }
    if (finded == 0)
    {
        fprintf(plik,"\n-----------\n");
        fprintf(plik,"Wartosc sredniej dla X wynosi: %f",m);
        fprintf(plik,"\nWartosc sredniej dla Y wynosi: %f", m2);
        fprintf(plik,"\nWartosc sredniej dla RHO wynosi: %f", m3);
        fprintf(plik,"\nMediana dla X wynosi: %f", me);
        fprintf(plik,"\nMediana dla Y wynosi: %f", me2);
        fprintf(plik,"\nMediana dla RHO wynosi: %f", me3);
        fprintf(plik, "\nWartosc odchhylenia standardowego dla X wynosi: %f", st);
        fprintf(plik, "\nWartosc odchhylenia standardowego dla Y wynosi: %f", st2);
        fprintf(plik, "\nWartosc odchhylenia standardowego dla RHO wynosi: %f", st3);
        fprintf(plik, "\n-----------");
    }
}
fclose(plik);
return 0;

}