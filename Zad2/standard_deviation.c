#include "funkcje.h"
#include <math.h>

float standard_deviation(int p_n, float p_A[], float p_mean)
{
    int i = 0;
    float odch = 0.0;

    for(i=0; i<p_n; i++)
    {
        odch += pow((p_A[i] - p_mean), 2);         
    }

    odch = sqrt(odch / p_n);

    return odch;
}